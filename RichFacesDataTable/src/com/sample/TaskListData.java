package com.sample;

public class TaskListData
{
    private String taskNode;
    private long   taskInstanceId;
    private String actorId;
    private String description;

    public String getActorId()        { return actorId; }
    public String getTaskNode()       { return taskNode; }
    public String getDescription()    { return description; }
    public long   getTaskInstanceId() { return taskInstanceId; }

    public void setActorId( String actorId )            { this.actorId = actorId; }
    public void setTaskNode( String currentNode )       { this.taskNode = currentNode; }
    public void setDescription( String description )    { this.description = description; }
    public void setTaskInstanceId( long taskInstanceId ){ this.taskInstanceId = taskInstanceId; }
}
