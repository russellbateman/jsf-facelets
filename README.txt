Notes on this set of projects...                                  1 April 2011

The source code all comes from my basic work to find a stable set of libraries
for use in writing JSF and Facelets applications. Merely import these projects
into Eclipse and add the libraries per articles:

http://www.javahotchocolate.com/tutorials/richfaces.html
http://www.javahotchocolate.com/tutorials/rf-facelets.html
http://www.javahotchocolate.com/tutorials/rf-simple.html
http://www.javahotchocolate.com/tutorials/rf-template.html
