package com.etretatlogiciels.facelets;

import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class UserNumberBean
{
    Integer      randomInt   = null;
    Integer      userNumber  = null;
    String       response    = null;
    private long maximum     = 10;
    private long minimum     = 0;

    public UserNumberBean()
    {
        Random randomGR = new Random();

        randomInt = new Integer( randomGR.nextInt( 10 ) );
        System.out.println( "Duke's number: " + randomInt );
    }

    public Integer getUserNumber() { return this.userNumber; }
    public long    getMaximum()    { return this.maximum; }
    public long    getMinimum()    { return this.minimum; }

    public void setUserNumber( Integer user_number ){ this.userNumber = user_number; }
    public void setMaximum( long maximum )          { this.maximum = maximum; }
    public void setMinimum( long minimum )          { this.minimum = minimum; }

    public String getResponse()
    {
        return( userNumber != null && userNumber.compareTo( randomInt ) == 0 )
             ? "Yay! You got it!"
             : "Sorry, " + userNumber + " is incorrect.";
    }
}
