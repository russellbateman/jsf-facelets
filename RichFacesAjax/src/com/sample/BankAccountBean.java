package com.sample;

public class BankAccountBean
{
    private String name;
    private String account;

    public String getAccount() { return account; }
    public void setAccount( String account ) { this.account = account; }

    public String getName() { return name; }
    public void setName( String name ) { this.name = name; }

    public void searchAccount() { account = name + "1234X"; }
}
