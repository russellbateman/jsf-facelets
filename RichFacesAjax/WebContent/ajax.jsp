<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<html>
<body>
<f:view>
	<a4j:form>
		<h:panelGrid columns="3">
			<h:outputText value="Name:" />
			<h:inputText value="#{bankAccount.name}" />
			<a4j:commandLink reRender="out" action="#{bankAccount.searchAccount}">
				<h:outputText value="Enter name" />
			</a4j:commandLink>
		</h:panelGrid>
	</a4j:form>

	<rich:spacer height="7" />
	<br />
	<h:panelGroup id="out">
		<h:outputText value="Your account is " rendered="#{not empty bankAccount.account}" />
		<h:outputText value="#{bankAccount.account}" />
		<h:outputText value="!" rendered="#{not empty bankAccount.account}" />
	</h:panelGroup>

</f:view>
</body>
</html>
