package com.etretatlogiciels.facelets.beans;

import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * This is brought in from another tutorial. The original reason even to do this RichFaces tutorial
 * was to provide a bed in which I could experiment with Facelets. </p>
 *
 * <pre>
 * <i>http://download.oracle.com/javaee/6/tutorial/doc/gipob.html</i>
 * </pre>
 *
 * <p>
 * I have not duplicated all the <i>web.xml</i> entries from that tutorial out of a desire not to
 * bork the hosting tutorial too much. Therefore, to active this demo, the URI is:
 * </p>
 *
 * <pre>
 * <i>http://localhost:8080/com.mastertheboss.richfaces/facelet-stuff/greeting.xhtml</i>
 * </pre>
 *
 * @author Russell Bateman
 */
@ManagedBean
@SessionScoped
public class UserNumberBean
{
    Integer      randomInt  = null;
    Integer      userNumber = null;
    String       response   = null;
    private long maximum    = 10;
    private long minimum    = 0;

    public UserNumberBean()
    {
        Random   randomGR = new Random();

        randomInt = new Integer( randomGR.nextInt( 10 ) );
        System.out.println( "Duke's number: " + randomInt );
    }

    public Integer getUserNumber(){ return userNumber; }
    public long    getMaximum()   { return( this.maximum ); }
    public long    getMinimum()   { return( this.minimum ); }

    public void setUserNumber( Integer user_number ){ userNumber = user_number; }
    public void setMaximum( long maximum )       { this.maximum = maximum; }
    public void setMinimum( long minimum )       { this.minimum = minimum; }

    public String getResponse()
    {
        if( userNumber != null && userNumber.compareTo( randomInt ) == 0 )
            return "Yay! You got it!";
        else
            return "Sorry, " + userNumber + " is incorrect.";
    }
}
